#!/bin/python3
'''
This script will generate a local archive of the content hosted on romhacking.net
Please do not spam romhacking.net, it is a nice website.

@Nightcrawler: If you're reading this, please know we did it with the best of intentions. We mean no harm.
'''

import os
import re
import sys
import wget
import time
import argparse
import codecs
import requests
import mechanize
from bs4 import BeautifulSoup
from tqdm import tqdm
#import inquirer

def sleep_time_validation(answers, current):
    try:
        n = int(current)
    except:
        # If unable to parse string as int, the answer is not interpreted as valid
        raise inquirer.errors.ValidationError('', reason='Invalid number')
    if n >= 0:
        return True
    else:
        raise inquirer.errors.ValidationError('', reason='Value must be >= 0')
    return False

def yes_no_question(answers, current):
    if current.lower() in ['y', 'n']:
        return True
    else:
        raise inquirer.errors.ValidationError('', reason='Please use \'y\' or \'n\'.')
    return False

def generate_info_file(hack_folder, info_dict):
    '''
    Dump info from info_dict into a readme-type file
    '''
    file_location = os.path.join(hack_folder, 'scraped_info.txt')
    with codecs.open(file_location, 'w', 'utf-8') as f:
        for key in info_dict:
            if key in ['rom_info', 'description']:
                # We will do these by hand
                continue
            f.write('## {:<25}'.format(key + ':'))
            f.write(str(info_dict[key]))
            f.write('\n')
        f.write('------------------------------------------\n')
        if 'description' in info_dict.keys():
            f.write('######## description ########\n')
            f.write(info_dict['description'])
        if 'rom_info' in info_dict.keys():
            f.write('\n\n######## rom_info ########\n')
            for info in info_dict['rom_info']:
                f.write('\t* ' + info.text + '\n')

def safe_str(string):
    '''
    Python freaks out when trying to create a folder or file with these characters, so we replace them
    by a '_' character
    '''
    return re.sub("[|,/,:,*,.,?\"]", "_", string)

def create_folder(info_dict, outputDir, dl_type, download_link):
    '''
    On local hard drive, create a folder that will organize the downloaded hacks
    '''
    # If the download_link is None, we save this in folder architecture in a 'MISSING' folder
    if download_link is None:
        outputDir = os.path.join(outputDir,'MISSING')

    if dl_type == 'hacks':
        folder = os.path.join(outputDir, dl_type, safe_str(info_dict['platform']),
                safe_str(info_dict['hack_of']),
                safe_str(info_dict['title'] + '___' + info_dict['patch_ver']))
    elif dl_type == 'translations':
        folder = os.path.join(outputDir, dl_type, safe_str(info_dict['platform']),
                safe_str(info_dict['title']), safe_str(info_dict['language']),
                safe_str(info_dict['released_by'] + '___' + info_dict['patch_ver']))
    else:   # homebrew
        folder = os.path.join(outputDir, dl_type, safe_str(info_dict['platform']),
                safe_str(info_dict['title']))

    os.makedirs(folder, exist_ok=True)
    return folder


def get_hacks_info(info_card):
    ret_dict = {}

    ret_dict['category'] = info_card.find('th', string='Category').find_next('a').text
    ret_dict['credits'] = [s.text for s in info_card.find_all('td', attrs={'class':'col_1 Contributor'})]
    ret_dict['description'] = info_card.find('h3', string='Description:').find_next('div').text
    ret_dict['genre'] = info_card.find('th', string='Genre').find_next('td').text
    ret_dict['hack_of'] = info_card.find('h4', attrs={'class':'date'}).text[8:]     # Remove 'Hack of '
    ret_dict['hack_release_date'] = info_card.find('th', string='Hack Release Date').find_next('td').text
    ret_dict['mods'] = info_card.find('th', string='Mods').find_next('td').text
    ret_dict['patch_ver'] = info_card.find('th', string='Patch Version').find_next('a').text
    ret_dict['patching_info'] = info_card.find('th', string='Patching Information').find_next('td').text
    ret_dict['rom_info'] = info_card.find('div', attrs={'id':'rom_info'}).find_all('li')

    return ret_dict

def get_homebrew_info(info_card):
    ret_dict = {}

    ret_dict['category'] = info_card.find('th', string='Category').find_next('a').text
    ret_dict['description'] = info_card.find('h3', string='Description:').find_next('div').text
    ret_dict['features'] = info_card.find('th', string='Features').find_next('td').text
    ret_dict['license'] = info_card.find('th', string='License').find_next('td').text
    ret_dict['release_date'] = info_card.find('th', string='Release Date').find_next('td').text
    ret_dict['source_code'] = info_card.find('th', string='Source Code').find_next('td').text
    ret_dict['source_lang'] = info_card.find('th', string='Source Language').find_next('td').text
    ret_dict['source_util'] = info_card.find('th', string='Source Utility').find_next('td').text
    ret_dict['version'] = info_card.find('th', string='Version').find_next('a').text

    return ret_dict

def get_translations_info(info_card):
    ret_dict = {}

    ret_dict['credits'] = [s.text for s in info_card.find_all('td', attrs={'class':'col_1 Contributor'})]
    ret_dict['game_date'] = info_card.find('th', string='Game Date').find_next('td').text
    ret_dict['description'] =  info_card.find('h3', string='Game Description:').find_next('div').text
    ret_dict['genre'] = info_card.find('th', string='Genre').find_next('td').text
    ret_dict['language'] = info_card.find('th', string='Language').find_next('a').text
    ret_dict['patch_ver'] = info_card.find('th', string='Patch Version').find_next('a').text
    ret_dict['patching_info'] = info_card.find('th', string='Patching Information').find_next('td').text
    ret_dict['published_by'] = info_card.find('th', string='Published By').find_next('td').text
    ret_dict['release_date'] = info_card.find('th', string='Release Date').find_next('td').text
    ret_dict['rom_info'] = info_card.find('div', attrs={'id':'rom_info'}).find_all('li')
    ret_dict['status'] = info_card.find('th', string='Status').find_next('a').text
    ret_dict['translation_desc'] = info_card.find('h3', string='Translation Description:').find_next('div').text

    return ret_dict


def download_hack(save_path, url):
    '''
    Pass the security test
    '''
    SESS_COOKIE_NAME = 'PHPSESSID'

    # Instanciate http session
    sess = requests.Session()

    # GET the content of the webpage
    req = sess.get(url)

    # Parse the response for cookie
    cookie = req.cookies.values()[0]
    cookies = {
        SESS_COOKIE_NAME: cookie
    }

    # Find the password on the returned HTML
    soup = BeautifulSoup(req.text, 'html.parser')
    password = soup.find('span', attrs={'class': 'note'}).get_text().split(' ')[-1][:-1]

    # Define POST body and headers
    body = {
        'passwrd': password,
        'id': url,
        'I_am_Human': 'I am Human',
        }
    headers = {
        'Referer': url, # Necessary to make the thing work
        }
    sess.headers.update(headers)

    # Infinite loop until we manage to successfully download
    while True:
        try:
            # POST the request (timeout after 60 seconds, most downloads happen within 10 seconds)
            response = sess.post(url, body, cookies=cookies, timeout=60)
            break
        except Exception as e:
            print("Download failed", url, "will try again:", e)

    # Parse the filename from headers
    filename = response.headers['Content-disposition'][22:][:-1]

    # Write the contents of the response to a file
    fileobj = open(os.path.join(save_path, filename), 'wb+')
    fileobj.write(response.content)
    fileobj.close()

if __name__ == '__main__':

    # Print error message and exit to prevent useless spam to rhdn servers.
    print("Since 20210921, RHDN has moved downloads behind user accounts.",
            "As a result, this scraper cannot work properly.",
            "Now exiting.")
    sys.exit(0)
    #######################################################################

    #################### PROGRAM BEGINS HERE ##############################
    # Define output directory
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', dest='outputdir', help='Where to save the scraped data', default='.')
    parser.add_argument('-d', dest='dl_type', help='What to download',
            choices=['hacks', 'translations', 'homebrew'])
    parser.add_argument('-t', dest='sleep_time',
            help='How much seconds of sleep between downloads (prevent spam)',
            default=5,
            type=int)
    parser.add_argument('-s', dest='skip_duplicates', help='Don\'t skip files already downloaded',
    action='store_true', default=True)

    args = parser.parse_args()
    output_dir = args.outputdir

    ### Interactive mode: this feature is currently disabled, but we leave the code here in case we want to
    #                     resurrect it.
    '''
    # Step 1: Determine which type of download user wants to do
    # (We only allow one type of download per execution, since romhacking.net is configured differently
    # based on the category of download)
    questions = [
        inquirer.List('dl_type',
            message='What do you want to download',
            choices=['hacks', 'translations', 'homebrew'],
        ),
        inquirer.Text('sleep_time',
            message='How much seconds of sleep between downloads to prevent spamming the website',
            default=5,
            validate=sleep_time_validation,
        ),
        inquirer.Text('skip_duplicates',
            message='Should I skip files that were already downloaded (y/n)',
            default='y',
            validate=yes_no_question,
        ),
    ]
    answers = inquirer.prompt(questions)
    '''
    dl_type = args.dl_type
    sleep_time = int(args.sleep_time)
    skip_duplicates = args.skip_duplicates
    dl_file_name = os.path.join(output_dir, dl_type, '.downloads.txt') # This file contains links of files already downloaded

    already_downloaded = []
    if skip_duplicates is True:
        try:
            with open(dl_file_name, 'r') as f:
                already_downloaded = f.read().splitlines()
        except Exception:
            # Don't print error, this only fails because it's the user's first download
            pass

    # Step 2: Generate a search query
    main_url = 'http://www.romhacking.net/{}/'.format(dl_type)
    br = mechanize.Browser()
    br.set_handle_robots(False)
    br.open(main_url)
    br.select_form(action = '/') # <form class='filter' method='get' action='/' accept-charset='UTF-8'>
    br['perpage'] = ['200'] # We want many results per page

    # Filter results by date, descending (newest results first), not available for homebrews though....
    if dl_type == 'hacks' or dl_type == 'translations':
        br['order'] = ['Date']
        br['dir'] = ['1']
    if dl_type == 'translations':
        # Select 'None Selected' for language
        br['languageid'] = [""]

    ### Interactive mode: this feature is currently disabled, but we leave the code here in case we want to
    #                     resurrect it.
    '''
    # We use beautifulsoup to find what are the available fields in the form
    soup_step2 = BeautifulSoup(br.response().read(), 'html.parser')
    # We care about two filters at the moment: Platform and Language (translations only)
    filters = ['platform', 'languageid'] if dl_type == 'translations' else ['platform']
    for filt in filters:
        options = soup_step2.find('select', attrs={'id': filt}).findChildren('option')
        options_str = [s.contents[0] for s in options]
        options_dict = {}
        for opt in options:
            # Create a mapping of the int matching the string of the filter (romhacking searches by id)
            options_dict[opt.text] = opt.attrs['value']

        questions = [
                inquirer.List(filt,
                    message='Filter by {} (use \'None Selected\' for all)'.format(filt),
                    choices=options_str,
                    ),
                ]
        select_filt = options_dict[inquirer.prompt(questions)[filt]]
        br[filt] = [select_filt]
    '''

    # Load the search query with our filters
    br.submit()

    # Step 3: In the results table, make a list of the links to all our search results.
    # If necessary, we will handle switching pages in the search results (because romhacking limits to 200
    # results per page)
    flipping_pages = True
    while flipping_pages is True:
        soup_step3 = BeautifulSoup(br.response().read(), 'html.parser')
        # Print how far we are in results page
        print(soup_step3.find('caption').text)
        title_columns = soup_step3.find_all('td', attrs={'class': 'col_1 Title'})
        links = ['http://www.romhacking.net' + l.findChild('a')['href'] for l in title_columns]

        # Check if we can click the 'Next' button to go to the next page...
        found_next_button = False
        page_buttons = soup_step3.find('div', attrs={'class': 'pages'}).findChildren('a')
        for button in page_buttons:
            if button.text == 'Next':
                found_next_button = True
                next_page_link = button['href']
            else:
                next_page_link = None

        # If after the loop above we still have not found a Next button, we stop the while loop.
        if found_next_button is False:
            flipping_pages = False

        # Step 4: Loop through the list of links retrieved in step 3, parse information for each download and create
        # folder for each download.
        if len(links) == 0:
            print('Your search returned 0 results!')
            sys.exit()

        for link in tqdm(links, desc='Downloading assets'):
            try:
                if skip_duplicates and link in already_downloaded:
                    # Skip hacks that were already downloaded if requested
                    print('Already downloaded, skip', link)
                    continue
                br.open(link)
                soup_step4 = BeautifulSoup(br.response().read(), 'html.parser')
                info_dict = {}  # This dict will contain all the info from the hack's page

                # Gather information (these fields are available for all types of downloads on romhacking.net)
                info_dict['title'] = soup_step4.find('div', attrs={'id':'main'}).findChild('div', attrs={'class': 'topbar'}).findChild('h2').text
                info_dict['rhdn_link'] = link
                info_dict['released_by'] = soup_step4.find('th', string='Released By').find_next('a').text
                info_dict['platform']= soup_step4.find('th', string='Platform').find_next('a').text
                info_dict['downloads'] = soup_step4.find('th', string='Downloads').find_next('td').text
                info_dict['last_modified'] = soup_step4.find('th', string='Last Modified').find_next('a').text

                # Attempt to get download link from the page
                download_href = None
                try:
                    download_href = soup_step4.find('h3', string='Links:').find_next('a', string='Download')['href']
                except:
                    # There probably was an error fetching the download link, but if so that's because it's
                    # not in the page (maybe it was removed by the author?).
                    pass

                # Measure score by counting n_recommended / n_reviews
                # Kinda shaky way to get the reviews, since I use 'parent' twice...
                review_parent = soup_step4.find('h2', string='User Review Information').parent.parent
                reviews = review_parent.find_all('tr', attrs={'class': 'even'})
                # ... and we have to get the 'odd' lines, too.
                reviews += review_parent.find_all('tr', attrs={'class': 'odd'})
                n_recommended = len([rev for rev in reviews if rev.find('td', attrs={'class': 'col_5'}).text == 'Yes'])
                n_reviews = len(reviews)
                info_dict['score'] = '{}/{}'.format(n_recommended, n_reviews)

                # Specific fields (depend on the type of hack)
                if dl_type == 'hacks':
                    info_dict.update(get_hacks_info(soup_step4))
                elif dl_type == 'homebrew':
                    info_dict.update(get_homebrew_info(soup_step4))
                else:   # Translations
                    info_dict.update(get_translations_info(soup_step4))

                # Step 4.1: Create folder structure for the hack, and a scraped_info.md file
                hack_folder = create_folder(info_dict, output_dir, dl_type, download_href)
                generate_info_file(hack_folder, info_dict)
                # Get original README
                readme_link = soup_step4.find('th', string='Readme').find_next('a')['href']
                if len(readme_link) > 0:
                    wget.download(readme_link, os.path.join(hack_folder, 'og_readme.txt'), bar=None)
                # Get original screenshots
                screenshot_links = [soup_step4.find('img', attrs={'alt':'Title Screen'})['src']]   # title screen
                img_elements = soup_step4.find('h3', string='Screenshots:').find_next('div', attrs={'class': 'center'}).find_all('img')
                screenshot_links += [img['src'] for img in img_elements]
                if len(screenshot_links) > 0:
                    for i, ss_link in enumerate(screenshot_links):
                        safe_title = safe_str(info_dict['title'])
                        ext = ss_link[-4:]
                        wget.download(ss_link, os.path.join(hack_folder, '{0}_screenshot{1}{2}'.format(safe_title, i, ext)), bar = None)

                # Step 5: Download the actual zip file containing the hack. Must pass human test.
                if download_href is not None:
                    download_link = 'http://www.romhacking.net{0}'.format(download_href)

                    download_hack(hack_folder, download_link)


                with open(dl_file_name, 'a+') as dl_file:
                    dl_file.write(info_dict['rhdn_link'] + '\n')

                time.sleep(sleep_time)

            # In case anything goes wrong when downloading the hack, we don't want scraper to quit.
            # Handle the exception.
            except Exception as e:
                print('There was an error downloading {}, moving on...'.format(link))
                print('The original error:', e)

        # We finished looping over all entries in the result table page, so we move on to next page
        if found_next_button is True:
            br.open(next_page_link)
