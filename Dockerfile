FROM python:3

RUN pip3 install mechanize tqdm bs4 wget argparse requests

# If you want to use the interactive mode, this library is required
#RUN pip3 install inquirer

COPY scraper.py /scraper.py

ENTRYPOINT ["python3", "scraper.py", "-o", "/mnt/mydata"]
