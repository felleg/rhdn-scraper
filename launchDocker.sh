sudo docker build -t scraper .
sudo docker run -it --rm -v `pwd`:/mnt/mydata/ scraper $@
# Revert file permissions
sudo chown $USER:$USER -R hacks > /dev/null 2>&1
sudo chown $USER:$USER -R homebrew > /dev/null 2>&1
sudo chown $USER:$USER -R translations > /dev/null 2>&1
