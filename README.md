# UPDATE ON THE STATUS OF THIS PROJECT

This project is put on indefinite hiatus. I (@felleg) have been in communication with the administrators of
RHDN and they explicitely disagree with the goal of this project. The RHDN download section has been updated
to require an account for downloads. Out of respect for the community they have built, I will not push this
project further. Here is a quote from RHDN:

> Due to recent excessive download attempts of the entire site, downloads are currently in bandwidth preservation mode for the remainder of the billing period. Unfortunately, downloads will require an account during this time. It is our wish to return downloads freely to everybody as soon as it becomes possible again.

**You are free to fork this project, but know that it could impact the survival of RHDN in the short term. I believe it is not a risk worth taking.**

# Original Readme

This project allows to create backups of the data hosted on RomHacking.net. The scraper has been tested to
work with the website's layout as of 20210914.

Here is a write-up about this project written by @felleg: https://felixleger.com/posts/2021/01/web-scraping-for-preservation/

The latest public archive is available here: https://archive.org/edit/rhdn-20210914

# Launching the program

There are two ways to launch the program.

## The first way (local python)

Assuming you have python3 and the necessary dependencies already installed on your computer, is to simply run:

```text
usage: scraper.py [-h] [-o OUTPUTDIR] [-d {hacks,translations,homebrew}] [-t SLEEP_TIME] [-s]

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUTDIR          Where to save the scraped data
  -d {hacks,translations,homebrew}
                        What to download
  -t SLEEP_TIME         How much seconds of sleep between downloads (prevent DDoS)
  -s                    Don't skip files already downloaded
```

## The second way (docker)

If you don't want to mess with installing dependencies on your computer, a Dockerfile is setup for your
convenience. Just make sure you already have docker installed (`sudo apt install docker.io`).

To launch the scraper in the docker container, do this command (first setup will take longer):

```bash
./launchDocker.sh
```

# TODO

These are the things that could be made better in the scraper:

* 20210926: RHDN has updated the security of its website as a result of this scraper. I (@felleg) do not wish to
  push this project further, as it is clear that administrators disagree with my actions. I have updated the
  script to print an information message and exit without attempting any download.

* 20210802: script for "auto patching" roms/games
* 20210802: script for generating hashes for needed games for later "auto patching"
* 20210802: add downloader for other sites, like: https://fantasyanime.com/index, http://info.sonicretro.org/Category:Hacks, https://www.smwcentral.net/?p=section&s=tools&u=0&g=0&n=1&o=downloads&d=desc, http://sega.c0.pl/romhacking_mods_translation.html, https://evilgames.eu/emulation.htm, http://www.baddesthacks.net/?page_id=63, https://www.pokemon-roms.co, https://metroidconstruction.com/index.php and consider links in: https://pastebin.com/edWKBJqn
* 20210802: wget creates duplicate files with (1) (ie if download failes, and script is running again later)
* 20210728: It seems like thte `wget.download()` command hangs indefinitely at time, which will force you to
  restart the scraper.
* 20210124: The scraper is not able to download updates to pre-downloaded hacks, they are currently skipped.
* 20210124: Output directory flag is not supported when using docker mode (i.e. must save in current dir always)
